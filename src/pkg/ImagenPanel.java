package pkg;

import java.awt.Graphics;
import javax.swing.ImageIcon;
import javax.swing.JPanel;


//creamos la clase heredada de JPanel
public class ImagenPanel extends JPanel {

    //Se crea un método cuyo parámetro debe ser un objeto Graphics
    @Override
    public void paint(Graphics grafico) {

        //Se selecciona la imagen que tenemos en el paquete
        ImageIcon Img = new ImageIcon(getClass().getResource("/pkg/imagen.jpg"));

        //se dibuja la imagen que tenemos en el paquete dentro del panel
        grafico.drawImage(Img.getImage(), 0, 0, getSize().width, getSize().height, null);
        setOpaque(false);
        paintComponent(grafico);
    }
}
